package jsilva.example.domain.domain

data class User
constructor(val id : Long,
            val name : String,
            val avatar : String,
            val followersCount : Int?,
            var followers : List<User> = emptyList())