package jsilva.example.domain.usecases

import jsilva.example.data.repositories.GithubUserRepository
import jsilva.example.domain.domain.User
import jsilva.example.data.datasource.PagedListEntity
import jsilva.example.domain.shared.PagedListModel
import jsilva.example.domain.usecase.BaseUseCase

/**
 * As a user I want to be able to look for users in Github given their username or password.
 */
class UseCaseSearchForUsers (private val repository: GithubUserRepository) : BaseUseCase<PagedListModel<User>, PagedListEntity<String>>() {

    override suspend fun run(params: PagedListEntity<String>): PagedListModel<User> {
        return PagedListModel(
                repository.lookForUsers(params)
        )

    }

}
