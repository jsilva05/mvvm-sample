package jsilva.example.domain.usecases

import jsilva.example.data.repositories.GithubUserRepository
import jsilva.example.domain.domain.User
import jsilva.example.domain.usecase.BaseUseCase


class UseCaseGetUserAndFollowers (private val repository: GithubUserRepository) : BaseUseCase<User, String>() {

    override suspend fun run(params: String): User {
        val user = repository.getUserByUsername(params)
        val followers = repository.getUserFollowers(params)

        user.followers = followers
        return user
    }
}
