package jsilva.example.data.network

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import okhttp3.Interceptor
import jsilva.example.data.network.mappers.UserMapper
import jsilva.example.data.repositories.GithubUserRepository
import jsilva.example.db.DatabaseManager
import jsilva.example.domain.domain.User
import jsilva.example.data.BaseServiceGenerator
import jsilva.example.data.NETWORK_PAGE_SIZE
import jsilva.example.data.datasource.BaseBoundaryCallback
import jsilva.example.data.datasource.PagedListEntity

class GithubService(override val baseUrl: String,
                    private val dbManager: DatabaseManager)
    : BaseServiceGenerator<GithubUsersApiService>(null), GithubUserRepository{

    init {
        build(null, null)
    }

    override val serviceClass: Class<GithubUsersApiService> = GithubUsersApiService::class.java


    override fun getInterceptors(): List<Interceptor> {
        return emptyList()
    }

    override suspend fun lookForUsers(params: PagedListEntity<String>) : LiveData<PagedList<User>> {

        dbManager.deleteAllUsers()

        val dataSourceFactory = dbManager.getAllUsers(params.params)

        val callback : BaseBoundaryCallback<GithubUsersPage, User> = BaseBoundaryCallback(
                {
                    api().getUsers(params.params, NETWORK_PAGE_SIZE, it).await()
                },
                {
                    dbManager.insertUsers(it.items)
                },
                params.scope
        )

        return buildLivePagedList(dataSourceFactory, callback)

    }

    override suspend fun getUserByUsername(username: String): User {

        return api().getUserDetails(username).await()
                .unwrapResponse()
                .let {
                    UserMapper().toModel(it)
                }
    }

    override suspend fun getUserFollowers(username: String): List<User> {

        return api().getFollowers(username).await()
                .unwrapResponse()
                .let {
                    UserMapper().toModelList(it)
                }
    }


}
