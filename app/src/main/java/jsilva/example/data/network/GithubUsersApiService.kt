package jsilva.example.data.network

import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GithubUsersApiService {

    @GET("search/users")
    fun getUsers(@Query("q") query : String, @Query("per_page") perPage: Int, @Query("page") page: Int) : Deferred<Response<GithubUsersPage>>

    @GET("users/{username}")
    fun getUserDetails(@Path("username") username : String) : Deferred<Response<GithubUserDto>>

    @GET("users/{username}/followers")
    fun getFollowers(@Path("username") username : String) : Deferred<Response<List<GithubUserDto>>>
}