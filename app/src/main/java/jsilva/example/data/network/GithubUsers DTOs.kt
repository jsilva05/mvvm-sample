package jsilva.example.data.network

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class GithubUserDto
constructor(@Expose @SerializedName("id")                   val id : Long,
            @Expose @SerializedName("login")                val login : String,
            @Expose @SerializedName("avatar_url")           val avatar : String,
            @Expose @SerializedName("followers")            val followers : Int?,
            @Expose @SerializedName("score")                val score : Float)

data class GithubUsersPage
constructor(@Expose @SerializedName("items")                val items : List<GithubUserDto>,
            @Expose @SerializedName("totalCount")           val count : Int,
            @Expose @SerializedName("incomplete_results")   val incomplete_results : Boolean)