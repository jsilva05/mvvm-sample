package jsilva.example.data.network.mappers

import jsilva.example.data.network.GithubUserDto
import jsilva.example.domain.domain.User
import jsilva.example.data.mappers.DTOMapper

class UserMapper : DTOMapper<User, GithubUserDto> {

    override fun toModel(dto: GithubUserDto, vararg extraInput: Any) = User(dto.id, dto.login, dto.avatar, dto.followers)

}