package jsilva.example.data.repositories

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import jsilva.example.domain.domain.User
import jsilva.example.data.datasource.PagedListEntity


interface GithubUserRepository {

    suspend fun lookForUsers(params : PagedListEntity<String>) : LiveData<PagedList<User>>

    suspend fun getUserByUsername(username : String) : User

    suspend fun getUserFollowers(username : String) : List<User>

}