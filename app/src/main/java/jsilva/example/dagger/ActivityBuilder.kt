package jsilva.example.dagger

import dagger.Module
import dagger.android.ContributesAndroidInjector
import jsilva.example.dagger.modules.FragmentModule
import jsilva.example.view.MainActivity

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun bindMainActivity() : MainActivity

}
