package jsilva.example.dagger.modules

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager
import dagger.Module
import dagger.Provides
import jsilva.example.db.AppDatabase
import jsilva.example.db.DatabaseManager
import jsilva.example.domain.protocols.KeyManager
import jsilva.example.domain.protocols.SharedPrefsHelper
import jsilva.example.domain.shared.KeyManagerImpl
import jsilva.example.domain.shared.SharedPreferencesHelperImpl
import java.util.concurrent.Executors
import javax.inject.Singleton

@Module
class DataModule {

    @Provides
    @Singleton
    fun provideSharedPreferences(app: Application) : SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(app)
    }

    @Provides
    @Singleton
    fun providePreferenceHelper(sharedPreferences: SharedPreferences) : SharedPrefsHelper {
        return SharedPreferencesHelperImpl(sharedPreferences)
    }

    @Provides
    @Singleton
    fun provideKeyManagerProvider() : KeyManager {
        return KeyManagerImpl()
    }

    @Provides @Singleton
    fun provideAppDatabase(app : Application) : AppDatabase {
        return AppDatabase.getInstance(app)
    }

    @Provides @Singleton
    fun provideDbManager(appDatabase: AppDatabase) : DatabaseManager {
        return DatabaseManager(appDatabase, Executors.newSingleThreadExecutor())
    }
}