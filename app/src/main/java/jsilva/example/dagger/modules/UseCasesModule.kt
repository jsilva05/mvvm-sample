package jsilva.example.dagger.modules

import dagger.Module
import dagger.Provides
import jsilva.example.data.repositories.GithubUserRepository
import jsilva.example.domain.usecases.UseCaseGetUserAndFollowers
import jsilva.example.domain.usecases.UseCaseSearchForUsers

@Module
class UseCasesModule {

    @Provides
    fun provideSearchForUsersUseCase(repository: GithubUserRepository) : UseCaseSearchForUsers{

        return UseCaseSearchForUsers(repository)
    }

    @Provides
    fun provideGetUserAndFollowersUseCase(repository: GithubUserRepository) : UseCaseGetUserAndFollowers {

        return UseCaseGetUserAndFollowers(repository)
    }

}