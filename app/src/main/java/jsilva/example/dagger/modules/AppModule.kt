package jsilva.example.dagger.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import jsilva.example.view.SampleApplication
import javax.inject.Singleton

@Module
class AppModule {

    @Provides @Singleton
    fun provideContext(app : SampleApplication) : Context {
        return app
    }

}