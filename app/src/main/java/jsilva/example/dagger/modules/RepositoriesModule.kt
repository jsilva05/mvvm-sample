package jsilva.example.dagger.modules

import dagger.Module
import dagger.Provides
import jsilva.example.data.network.GithubService
import jsilva.example.data.repositories.GithubUserRepository
import jsilva.example.db.DatabaseManager
import javax.inject.Named
import javax.inject.Singleton

@Module(includes = [AppModule::class])
class RepositoriesModule {

    @Provides @Named("github-api") fun provideGithubApiUrl() : String = "https://api.github.com/"

    @Provides @Singleton
    fun provideGithubUsersRepository(@Named("github-api") url : String,
                                     dbManager: DatabaseManager) : GithubUserRepository {
        return GithubService(url, dbManager)
    }
}