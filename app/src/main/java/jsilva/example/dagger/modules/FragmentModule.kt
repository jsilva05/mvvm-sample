package jsilva.example.dagger.modules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import jsilva.example.view.details.DetailsFragment
import jsilva.example.view.details.DetailsModule
import jsilva.example.view.home.HomeFragment
import jsilva.example.view.home.HomeModule

@Module
abstract class FragmentModule {

    @ContributesAndroidInjector(modules = [HomeModule::class])
    abstract fun contributeHomeFragment(): HomeFragment

    @ContributesAndroidInjector(modules = [DetailsModule::class])
    abstract fun contributeDetailsFragment(): DetailsFragment
}