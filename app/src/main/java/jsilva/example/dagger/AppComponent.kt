package jsilva.example.dagger

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import jsilva.example.dagger.modules.AppModule
import jsilva.example.dagger.modules.DataModule
import jsilva.example.dagger.modules.RepositoriesModule
import jsilva.example.dagger.modules.UseCasesModule
import jsilva.example.view.SampleApplication
import javax.inject.Singleton


@Singleton
@Component(modules = [
    AndroidInjectionModule::class,
    AppModule::class,
    DataModule::class,
    RepositoriesModule::class,
    UseCasesModule::class,
    ActivityBuilder::class])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: SampleApplication)

}
