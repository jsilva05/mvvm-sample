package jsilva.example.view.home

import androidx.lifecycle.ViewModelProviders
import dagger.Module
import dagger.Provides
import jsilva.example.dagger.modules.UseCasesModule
import jsilva.example.domain.usecases.UseCaseSearchForUsers

@Module(includes = [UseCasesModule::class])
class HomeModule {

    @Provides
    fun provideViewModel(view : HomeFragment, searchForUsers: UseCaseSearchForUsers) : HomeViewModel {
        val viewModel = ViewModelProviders.of(view).get(HomeViewModel::class.java)

        viewModel.searchForUsersUseCase = searchForUsers
        return viewModel
    }

}