package jsilva.example.view.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.PagedList
import jsilva.example.domain.domain.User
import jsilva.example.domain.usecases.UseCaseSearchForUsers
import jsilva.example.data.datasource.PagedListEntity
import jsilva.example.domain.shared.PagedListModel
import jsilva.example.view.BaseViewModel

class HomeViewModel : BaseViewModel(){

    lateinit var searchForUsersUseCase : UseCaseSearchForUsers

    private val searchInput = MutableLiveData<String>()
    private val searchQuery = MutableLiveData<String>()

    private val searchResult = Transformations.switchMap(searchQuery) {

        val liveData = MutableLiveData<PagedListModel<User>>()

        executeUseCaseInForeground(searchForUsersUseCase, PagedListEntity(it, this))
        { repoResult ->
            liveData.postValue(repoResult)
        }

        liveData
    }

    val usersResult : LiveData<PagedList<User>?> = Transformations.switchMap(searchResult) {
        it.data
    }

    fun onSearchInputChanged(text : CharSequence){
        searchInput.postValue(text.toString())
    }

    fun onSearchUserPressed(){
        val searchValue = searchInput.value

        if (!searchValue.isNullOrEmpty()) {

            searchQuery.postValue(searchValue)

        }
    }
}
