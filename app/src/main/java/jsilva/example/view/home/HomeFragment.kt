package jsilva.example.view.home

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_home.*

import jsilva.example.blueprint.BR
import jsilva.example.blueprint.R
import jsilva.example.blueprint.databinding.FragmentHomeBinding
import jsilva.example.blueprint.databinding.ItemUserOptionBinding
import jsilva.example.domain.domain.User
import jsilva.example.view.BaseFragment
import jsilva.example.view.adapters.BasePagedListAdapter
import javax.inject.Inject

class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>() {

    @Inject
    lateinit var mViewModel: HomeViewModel

    override fun getLayoutId() = R.layout.fragment_home

    override fun getViewModel() = mViewModel

    override fun getBindingVariable() = BR.viewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = UserAdapter(findNavController())

        rv_users.setHasFixedSize(true)
        rv_users.layoutManager = LinearLayoutManager(context)
        rv_users.adapter = adapter

        mViewModel.usersResult.observe(this, Observer {

            if (it != null)
                adapter.swapDataset(it)

        })
    }

    class UserAdapter(private val navController: NavController) : BasePagedListAdapter<ItemUserOptionBinding, User>(DIFF){

        override fun onItemClicked(model: User) {

            val gotoDetailsAction = HomeFragmentDirections.gotoDetails()
            gotoDetailsAction.username = model.name

            navController.navigate(gotoDetailsAction)
        }

        override fun getViewLayoutResourceId(): Int = R.layout.item_user_option

        override fun getBindingVariable(): Int = BR.user


        companion object {
            val DIFF = object : DiffUtil.ItemCallback<User>() {

                override fun areItemsTheSame(oldItem: User, newItem: User)= oldItem.id == newItem.id

                override fun areContentsTheSame(oldItem: User, newItem: User) = oldItem.avatar == newItem.avatar
                        && oldItem.followersCount == newItem.followersCount
                        && oldItem.name == newItem.name

            }
        }
    }
}