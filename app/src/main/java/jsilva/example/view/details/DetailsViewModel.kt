package jsilva.example.view.details

import androidx.lifecycle.MutableLiveData
import androidx.databinding.ObservableField
import jsilva.example.domain.domain.User
import jsilva.example.domain.usecases.UseCaseGetUserAndFollowers
import jsilva.example.view.BaseViewModel

class DetailsViewModel : BaseViewModel() {

    val username = ObservableField<String>()
    val avatarUrl= ObservableField<String>()

    val followers = MutableLiveData<List<User>>()

    lateinit var getUserAndFollowers: UseCaseGetUserAndFollowers

    fun loadUserAndFollower(username: String?) {

        username?.let {

            executeUseCaseInForeground(getUserAndFollowers, username) { result ->
                this.username.set(result.name)
                this.avatarUrl.set(result.avatar)

                followers.postValue(result.followers)
            }

        }
    }
}