package jsilva.example.view.details

import androidx.lifecycle.ViewModelProviders
import dagger.Module
import dagger.Provides
import jsilva.example.domain.usecases.UseCaseGetUserAndFollowers

@Module
class DetailsModule {

    @Provides
    fun provideViewModel(view : DetailsFragment,
                         getUserAndFollowers: UseCaseGetUserAndFollowers) : DetailsViewModel {

        val viewModel = ViewModelProviders.of(view).get(DetailsViewModel::class.java)

        viewModel.getUserAndFollowers = getUserAndFollowers
        return viewModel
    }
}