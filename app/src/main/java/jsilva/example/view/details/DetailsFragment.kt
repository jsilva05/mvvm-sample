package jsilva.example.view.details

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_details.*
import jsilva.example.blueprint.R
import jsilva.example.blueprint.databinding.FragmentDetailsBinding
import jsilva.example.blueprint.databinding.ItemUserOptionBinding
import jsilva.example.domain.domain.User
import jsilva.example.blueprints.BR
import jsilva.example.view.BaseFragment
import jsilva.example.view.adapters.BaseRecyclerAdapter
import javax.inject.Inject

class DetailsFragment : BaseFragment<FragmentDetailsBinding, DetailsViewModel>() {

    override fun getLayoutId() = R.layout.fragment_details

    @Inject
    lateinit var mViewModel : DetailsViewModel

    override fun getViewModel() = mViewModel

    override fun getBindingVariable() = BR.viewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null) {

            val username = DetailsFragmentArgs.fromBundle(arguments!!).username

            if (username.isNotEmpty()) {

                mViewModel.loadUserAndFollower(username)

                val followersAdapter = FollowersAdapter()

                rv_followers.setHasFixedSize(true)
                rv_followers.layoutManager = LinearLayoutManager(context)
                rv_followers.adapter = followersAdapter

                mViewModel.followers.observe(this, Observer {
                    followersAdapter.swapDataset(it ?: emptyList())
                })

            }
        }
    }

    class FollowersAdapter : BaseRecyclerAdapter<ItemUserOptionBinding, User>() {

        override fun areItemsTheSame(oldItem: User, newItem: User) = oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: User, newItem: User) = oldItem.avatar == newItem.avatar
                && oldItem.followersCount == newItem.followersCount
                && oldItem.name == newItem.name

        override fun getViewLayoutResourceId(): Int = R.layout.item_user_option

        override fun getBindingVariable(): Int = BR.user

        override fun onItemClicked(model: User) {}

    }
}