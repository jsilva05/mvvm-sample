package jsilva.example.view

import jsilva.example.blueprint.R
import jsilva.example.view.BaseActivity

class MainActivity : BaseActivity() {

    override fun getProgressBarId() = R.id.pb_progress

    override fun getLayoutId() = R.layout.activity_main

}