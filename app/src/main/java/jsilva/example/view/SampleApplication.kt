package jsilva.example.view

import android.app.Activity
import android.app.Service
import com.facebook.stetho.Stetho
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.HasServiceInjector
import jsilva.example.blueprint.BuildConfig
import jsilva.example.dagger.DaggerAppComponent
import javax.inject.Inject

class SampleApplication : BlueprintApplication(), HasActivityInjector, HasServiceInjector{

    @Inject internal lateinit var mActivityInjector: DispatchingAndroidInjector<Activity>
    @Inject internal lateinit var mServiceInjector: DispatchingAndroidInjector<Service>

    override fun activityInjector(): AndroidInjector<Activity> = mActivityInjector

    override fun serviceInjector(): AndroidInjector<Service> = mServiceInjector


    override fun onCreate() {
        super.onCreate()

        DaggerAppComponent.builder().application(this).build().inject(this);

        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this);
        }
    }

}