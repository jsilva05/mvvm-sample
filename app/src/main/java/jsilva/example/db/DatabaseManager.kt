package jsilva.example.db

import androidx.paging.DataSource
import jsilva.example.data.network.GithubUserDto
import jsilva.example.db.mappers.UserFromDbMapper
import jsilva.example.db.mappers.UserToDbMapper
import jsilva.example.domain.domain.User
import java.util.concurrent.Executor

class DatabaseManager(private val database: AppDatabase,
                      private val executor: Executor) {

    fun getAllUsers(name: String): DataSource.Factory<Int, User> {
        val query = "%${name.replace(' ', '%')}%"

        return database.userDao().findByName(query)
                .mapByPage {
                    UserFromDbMapper().toModelList(it)
                }
    }

    fun insertUsers(users: List<GithubUserDto>) {
        executor.execute {
            database.userDao().insert(
                    UserToDbMapper().toModelList(users)
            )
        }
    }

    fun deleteAllUsers() {
        database.userDao().deleteAll()
    }
}