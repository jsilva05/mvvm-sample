package jsilva.example.db.mappers

import jsilva.example.data.network.GithubUserDto
import jsilva.example.db.tables.UserEntity
import jsilva.example.domain.domain.User
import jsilva.example.data.mappers.DTOMapper


class UserToDbMapper : DTOMapper<UserEntity, GithubUserDto> {
    override fun toModel(dto: GithubUserDto, vararg extraInput: Any) = UserEntity(dto.id, dto.login, dto.avatar, dto.followers)
}

class UserFromDbMapper : DTOMapper<User, UserEntity> {
    override fun toModel(dto: UserEntity, vararg extraInput: Any) = User(dto.id, dto.login, dto.avatar_url, dto.followers)
}