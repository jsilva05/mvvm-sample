package jsilva.example.db.tables

import androidx.paging.DataSource
import androidx.room.*

@Entity
data class UserEntity(
        var id: Long,
        val login: String,
        val avatar_url: String,
        val followers: Int?,
        @PrimaryKey(autoGenerate = true) val position: Int? = null
)

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(users: List<UserEntity>)

    @Query("select * from userentity where login like :name order by position")
    fun findByName(name: String) : DataSource.Factory<Int, UserEntity>

    @Query("delete from userentity")
    fun deleteAll()
}