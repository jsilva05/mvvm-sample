package jsilva.example.data

import retrofit2.HttpException
import retrofit2.Response

open class BaseService {

    fun <T> Response<T>.unwrapResponse(): T {
        if (isSuccessful) {
            val body = body()

            if (body != null)
                return body
        }

        throw HttpException(this)
    }

    open fun unwrapResponseAsBoolean(response: Response<*>): Boolean? {
        return response.isSuccessful
    }

}