package jsilva.example.data.datasource

import androidx.paging.PagedList
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import retrofit2.HttpException
import retrofit2.Response

class BaseBoundaryCallback<APIMODEL, MODEL>(
        private val apiCall: suspend (Int) -> Response<APIMODEL>,
        private val dbInsert: (APIMODEL) -> Unit,
        private val scope: CoroutineScope) : PagedList.BoundaryCallback<MODEL>() {

    private var lastRequestedPage = 1

    override fun onZeroItemsLoaded() {
        requestData()
    }

    override fun onItemAtEndLoaded(itemAtEnd: MODEL) {
        requestData()
    }

    private fun requestData() {

        scope.async {

            val response = apiCall(lastRequestedPage)
            if (response.isSuccessful) {

                val body = response.body()

                if (body != null) {
                    dbInsert(body)
                    lastRequestedPage++

                    return@async
                }
            }

            throw HttpException(response)
        }

    }
}