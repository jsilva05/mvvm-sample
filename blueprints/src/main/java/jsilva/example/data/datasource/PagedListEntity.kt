package jsilva.example.data.datasource

import kotlinx.coroutines.CoroutineScope

class PagedListEntity<MODEL>(
        val params: MODEL,
        val scope: CoroutineScope
)