package jsilva.example.data

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import jsilva.example.blueprints.BuildConfig
import timber.log.Timber
import java.net.Proxy
import java.security.KeyManagementException
import java.security.NoSuchAlgorithmException
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.*

const val NETWORK_READ_WRITE_TIMEOUT_IN_SECONDS = 60
const val NETWORK_CONNECT_TIMEOUT_IN_SECONDS    = 15
const val PAGINATION_PAGE_SIZE = 20
const val NETWORK_PAGE_SIZE = PAGINATION_PAGE_SIZE * 2

/**
 * Base class for all network services. A [BaseServiceGenerator] creates service that conforms
 * to the [API] interface. Ideally, a BaseServiceGenerator should also conform to one or more
 * repository-based protocols.
 *
 * All implementations of BaseServiceGenerator should be distributed through dependency injection.
 * Furthermore, **it's critical that all implementations of this class run the [BaseServiceGenerator.build]
 * on their constructor.
 */
abstract class BaseServiceGenerator<API>(protected val cache : Cache?) : BaseService() {

    protected lateinit var httpClient: OkHttpClient
        private set
    private var mService: API? = null

    private var mRetrofit: Retrofit? = null

    protected abstract val serviceClass: Class<API>

    protected abstract val baseUrl: String

    protected abstract fun getInterceptors(): List<Interceptor>

    fun runAsMock(mockService: API) {
        mService = mockService
    }

    protected fun build(sslSocketFactory: SSLSocketFactory?, trustManager: X509TrustManager?) {
        httpClient = prepareOkHttpClient(sslSocketFactory, trustManager)
        mRetrofit = prepareRetrofit()
    }

    protected fun api(): API {
        if (mService == null) {
            mService = createService(serviceClass)
        }
        return mService!!
    }

    private fun createService(serviceClass: Class<API>): API {
        return if (mRetrofit == null)
            throw RuntimeException(this.javaClass.simpleName + " must run build() in its constructor.")
        else
            mRetrofit!!.create(serviceClass)
    }


    private fun prepareRetrofit(): Retrofit {
        val gson = GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create()

        return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .client(httpClient)
                .build()
    }

    private fun prepareOkHttpClient(sslSocketFactory: SSLSocketFactory?,
                                    trustManager: X509TrustManager?): OkHttpClient {

        var builder: OkHttpClient.Builder = OkHttpClient.Builder()
                .cache(cache)
                .readTimeout(NETWORK_READ_WRITE_TIMEOUT_IN_SECONDS.toLong(), TimeUnit.SECONDS)
                .writeTimeout(NETWORK_READ_WRITE_TIMEOUT_IN_SECONDS.toLong(), TimeUnit.SECONDS)
                .connectTimeout(NETWORK_CONNECT_TIMEOUT_IN_SECONDS.toLong(), TimeUnit.SECONDS)
        //.certificatePinner(buildCertificatePinner());

        //Disable Proxy if necessary
        if (disableProxy()) builder.proxy(Proxy.NO_PROXY)

        //if (withConnectionSpecs() != null) builder.connectionSpecs(listOf(withConnectionSpecs()))

        if (sslSocketFactory != null && trustManager != null)
            builder = builder.sslSocketFactory(sslSocketFactory, trustManager)

        for (i in getInterceptors())
            builder.addInterceptor(i)

        if (BuildConfig.DEBUG) {
            val logInterceptor = HttpLoggingInterceptor { message -> Timber.d(message) }
            logInterceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(logInterceptor)
        }


        if (allowUntrustedHost()) {
            Timber.w("**** Allow untrusted SSL connection ****")
            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                override fun getAcceptedIssuers(): Array<X509Certificate> {
                    return emptyArray()
                }

                @Throws(CertificateException::class)
                override fun checkServerTrusted(chain: Array<X509Certificate>,
                                                authType: String) {
                }

                @Throws(CertificateException::class)
                override fun checkClientTrusted(chain: Array<X509Certificate>,
                                                authType: String) {
                }
            })

            val sslContext: SSLContext

            try {
                sslContext = SSLContext.getInstance("SSL")
                sslContext.init(null, trustAllCerts, java.security.SecureRandom())
            } catch (e: KeyManagementException) {
                throw RuntimeException(e.cause)
            } catch (e: NoSuchAlgorithmException) {
                throw RuntimeException(e.cause)
            }

            if (trustManager != null)
                builder.sslSocketFactory(sslContext.socketFactory, trustManager)

            val hostnameVerifier = HostnameVerifier{ hostname, _ ->
                Timber.d("Trust Host :%s", hostname)
                true
            }

            builder.hostnameVerifier(hostnameVerifier)
        }


        return builder.build()
    }

    protected open fun allowUntrustedHost(): Boolean {
        return false
    }

    protected open fun disableProxy(): Boolean {
        return false
    }

    protected fun <ENTITYMODEL> buildLivePagedList(datasource : DataSource.Factory<Int, ENTITYMODEL>, callback : PagedList.BoundaryCallback<ENTITYMODEL>)
            : LiveData<PagedList<ENTITYMODEL>> {

        val config = PagedList.Config.Builder()
                .setPageSize(PAGINATION_PAGE_SIZE)
                .setEnablePlaceholders(true)
                .build()

        return LivePagedListBuilder(datasource, config)
                .setBoundaryCallback(callback)
                .build()

    }
}