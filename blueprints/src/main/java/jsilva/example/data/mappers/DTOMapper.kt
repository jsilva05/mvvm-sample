package jsilva.example.data.mappers

/**
 * Protocol that specifies how to convert a DTO [D] into a single or a list of models [M].
 */
interface DTOMapper<M, D> {

    /**
     * Converts a DTO [D] into a model [M]
     * @param dto The DTO
     * @return the converted model [M]
     */
    fun toModel(dto : D, vararg extraInput: Any) : M

    /**
     * Converts a List of [D] into a List of [M]
     */
    fun toModelList(dtos : List<D>, vararg extraInput: Any) : List<M> {
        return dtos.map { toModel(it, extraInput) }
    }
}
