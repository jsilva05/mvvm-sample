package jsilva.example.domain.exception

open class BaseException private constructor(
        val errorCode: Int,
        val errorMessage: String? = null,
        val errorMessageId: Int? = null
) : Exception() {

    constructor(errorCode: Int, errorMessage: String) : this (errorCode, errorMessage, null)

    constructor(errorCode: Int, errorMessageId: Int) : this (errorCode, null, errorMessageId)
}