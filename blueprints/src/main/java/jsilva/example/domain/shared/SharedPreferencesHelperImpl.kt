package jsilva.example.domain.shared

import android.content.SharedPreferences
import jsilva.example.domain.protocols.SharedPrefsHelper

class SharedPreferencesHelperImpl(private val mSharedPreferences: SharedPreferences) : SharedPrefsHelper {

    override fun put(key: String, value: String) {
        mSharedPreferences.edit().putString(key, value).apply()
    }

    override fun put(key: String, value: Int) {
        mSharedPreferences.edit().putInt(key, value).apply()
    }

    override fun put(key: String, value: Float) {
        mSharedPreferences.edit().putFloat(key, value).apply()
    }

    override fun put(key: String, value: Boolean) {
        mSharedPreferences.edit().putBoolean(key, value).apply()
    }

    override fun getString(key: String, defaultValue: String?): String? {
        return mSharedPreferences.getString(key, defaultValue)
    }

    override fun getInt(key: String, defaultValue: Int?): Int? {
        val intValue = mSharedPreferences.getInt(key, defaultValue ?: Int.MIN_VALUE)
        return if (intValue == Int.MIN_VALUE) null else intValue
    }

    override fun getFloat(key: String, defaultValue: Float?): Float? {
        val floatValue = mSharedPreferences.getFloat(key, defaultValue ?: Float.NaN)
        return if (floatValue == Float.NaN) null else floatValue
    }

    override fun getBoolean(key: String, defaultValue: Boolean): Boolean {
        return mSharedPreferences.getBoolean(key, defaultValue)
    }

    override fun deleteSavedData(key: String) {
        mSharedPreferences.edit().remove(key).apply()
    }

}