package jsilva.example.domain.shared

import androidx.lifecycle.LiveData
import androidx.paging.PagedList

data class PagedListModel<MODEL>(
        val data: LiveData<PagedList<MODEL>>?
)