package jsilva.example.domain.shared

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.security.KeyPairGeneratorSpec
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import jsilva.example.domain.protocols.KeyManager
import timber.log.Timber
import java.lang.RuntimeException
import java.math.BigInteger
import java.security.*
import java.security.spec.RSAKeyGenParameterSpec
import java.security.spec.RSAKeyGenParameterSpec.F4
import java.util.*
import javax.crypto.Cipher
import javax.crypto.NoSuchPaddingException
import javax.security.auth.x500.X500Principal
import android.util.Base64

private const val KEYSTORE_PROVIDER_ANDROID_KEYSTORE = "AndroidKeyStore"
private const val TYPE_RSA = "RSA"
private const val PADDING_TYPE = "PKCS1Padding"
private const val BLOCKING_MODE = "NONE"

class KeyManagerImpl : KeyManager {

    @Throws(NoSuchProviderException::class, NoSuchAlgorithmException::class, InvalidAlgorithmParameterException::class, RuntimeException::class)
    override fun createKeys(context: Context, alias: String) {
        if (!isSigningKey(alias)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                createKeysM(alias, false)
            else
                createKeysMR2(context, alias)
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    override fun createKeysM(alias: String, requireAuth: Boolean) {
        try {

            val keyPairGenerator = KeyPairGenerator.getInstance(
                    KeyProperties.KEY_ALGORITHM_RSA, KEYSTORE_PROVIDER_ANDROID_KEYSTORE
            )

            keyPairGenerator.initialize(
                    KeyGenParameterSpec.Builder(
                            alias,
                            KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
                            .setAlgorithmParameterSpec(RSAKeyGenParameterSpec(1024, F4))
                            .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                            .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_PKCS1)
                            .setDigests(KeyProperties.DIGEST_SHA256,
                                    KeyProperties.DIGEST_SHA384,
                                    KeyProperties.DIGEST_SHA512)
                            .setUserAuthenticationRequired(requireAuth)
                            .build()
            )

            keyPairGenerator.generateKeyPair()


        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    override fun createKeysMR2(context: Context, alias: String) {
        val start = Calendar.getInstance()
        val end = Calendar.getInstance()
        end.add(Calendar.YEAR, 30)

        val spec = KeyPairGeneratorSpec.Builder(context)
                .setAlias(alias)
                .setSubject(X500Principal("CN=$alias"))
                .setSerialNumber(BigInteger.valueOf(Math.abs(alias.hashCode().toLong())))
                .setStartDate(start.time)
                .setEndDate(end.time)
                .build()

        val keyPairGenerator = KeyPairGenerator.getInstance(
                TYPE_RSA,
                KEYSTORE_PROVIDER_ANDROID_KEYSTORE
        )
        keyPairGenerator.initialize(spec)
        keyPairGenerator.generateKeyPair()

    }

    override fun isSigningKey(alias: String) : Boolean {
        return try {
            val keyStore = KeyStore.getInstance(KEYSTORE_PROVIDER_ANDROID_KEYSTORE)
            keyStore.load(null)
            keyStore.containsAlias(alias)
        } catch (e: Exception) {
            Timber.e(e)
            false
        }
    }

    override fun encrypt(alias: String, plainText: String) : String {
        return try {

            val privateKey = getPrivateKeyEntry(alias)
            if (privateKey != null) {

                val publicKey = privateKey.certificate.publicKey
                val cipher = getCypher()
                cipher.init(Cipher.ENCRYPT_MODE, publicKey)

                return Base64.encodeToString(cipher.doFinal(plainText.toByteArray()), Base64.NO_WRAP)

            }

            ""
        } catch (ex: Exception) {
            Timber.e(ex, "Unable to encrypt password")
            ""
        }
    }

    override fun decrypt(alias: String, cipherText: String) : String? {

        return try {

            val privateKeyEntry = getPrivateKeyEntry(alias)
            if (privateKeyEntry != null) {

                val privateKey = privateKeyEntry.privateKey

                val cipher = getCypher()
                cipher.init(Cipher.DECRYPT_MODE, privateKey)
                return String(cipher.doFinal(Base64.decode(cipherText, Base64.NO_WRAP)))

            }

            null
        } catch (ex: Exception) {
            Timber.e(ex, "Unable to decrypt password")
            null
        }

    }

    private fun getPrivateKeyEntry(alias: String) : KeyStore.PrivateKeyEntry? {
        return try {

            val keyStore = KeyStore.getInstance(KEYSTORE_PROVIDER_ANDROID_KEYSTORE)
            keyStore.load(null)
            val entry = keyStore.getEntry(alias, null)

            if (entry == null || entry !is KeyStore.PrivateKeyEntry) {
                Timber.w("Error getting entry")
                return null
            }

            return entry

        } catch (ex: Exception) {
            Timber.e(ex)
            null
        }
    }

    @Throws(NoSuchPaddingException::class, NoSuchAlgorithmException::class)
    private fun getCypher() = Cipher.getInstance(
            "$TYPE_RSA/$BLOCKING_MODE/$PADDING_TYPE"
    )
}