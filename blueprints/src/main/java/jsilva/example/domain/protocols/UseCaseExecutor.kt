package jsilva.example.domain.protocols

import jsilva.example.domain.usecase.BaseUseCase

interface UseCaseExecutor {

    fun errorHandler(isForeground: Boolean, dismissListener: (() -> Unit)? = null) : (Throwable) -> Unit

    fun onCompleteHandler(isForeground: Boolean) : () -> Unit

    /* Foreground
     ************************************************************************************************/
    /**
     * Executes the provided use case with the input parameter, with foreground feedback, i.e. loading.
     * @param useCase The [BaseUseCase] use-case to be executed.
     * @param input The input parameters
     * @param onSuccess [Consumer] that specified the on success behavior.
     * @param <T> The [BaseUseCase] output generic type.
     * @param <P> The [BaseUseCase] input generic type.
    </P></T> */
    fun <T, P> executeUseCaseInForeground(useCase: BaseUseCase<T, P>, input: P, onSuccess: (T) -> Unit) {
        executeUseCase(useCase, input, onSuccess, errorHandler(true), onCompleteHandler(true), true)
    }

    /**
     * Executes the provided use case with the input parameter, with foreground feedback, i.e. loading.
     * @param useCase The [BaseUseCase] use-case to be executed.
     * @param input The input parameters
     * @param onSuccess [Consumer] that specifies the on success behavior.
     * @param onError [Consumer] that specifies the on error behavior.
     * @param <T> The [BaseUseCase] output generic type.
     * @param <P> The [BaseUseCase] input generic type.
    </P></T> */
    fun <T, P> executeUseCaseInForeground(useCase: BaseUseCase<T, P>, input: P, onSuccess: (T) -> Unit, onError: (Throwable) -> Unit){
        executeUseCase(useCase, input, onSuccess, onError, onCompleteHandler(true), true)
    }

    fun <T, P> executeUseCaseInForeground(useCase: BaseUseCase<T, P>, input: P, onSuccess: (T) -> Unit, onError: (Throwable) -> Unit, onComplete: () -> Unit){
        executeUseCase(useCase, input, onSuccess, onError, onComplete, true)
    }

    /* Background
     ************************************************************************************************/
    /**
     * Executes the provided use case with the input parameter in the background
     * @param useCase The [BaseUseCase] use-case to be executed.
     * @param input The input parameters
     * @param onSuccess [Consumer] that specifies the on success behavior.
     * @param onError [Consumer] that specifies the on error behavior.
     * @param <T> The [BaseUseCase] output generic type.
     * @param <P> The [BaseUseCase] input generic type.
    </P></T> */
    fun <T, P> executeUseCaseInBackground(useCase: BaseUseCase<T, P>, input: P, onSuccess: (T) -> Unit, onError: (Throwable) -> Unit){
        executeUseCase(useCase, input, onSuccess, onError, onCompleteHandler(false), false)
    }

    /**
     * Executes the provided use case with the input parameter in the background
     * @param useCase The [BaseUseCase] use-case to be executed.
     * @param input The input parameters
     * @param onSuccess [Consumer] that specifies the on success behavior.
     * @param <T> The [BaseUseCase] output generic type.
     * @param <P> The [BaseUseCase] input generic type.
    </P></T> */
    fun <T, P> executeUseCaseInBackground(useCase: BaseUseCase<T, P>, input: P, onSuccess: (T) -> Unit){
        executeUseCase(useCase, input, onSuccess, errorHandler(false), onCompleteHandler(false), false)
    }

    fun <T, P> executeUseCaseInBackground(useCase: BaseUseCase<T,P>, input: P, onSuccess: (T) -> Unit, onError: (Throwable) -> Unit, onComplete: () -> Unit) {
        executeUseCase(useCase, input, onSuccess, onError, onComplete, false)
    }

    fun <T, P> executeUseCase(useCase: BaseUseCase<T, P>,
                                       input: P,
                                       onSuccess: (T) -> Unit,
                                       onError: (Throwable) -> Unit,
                                       onComplete: () -> Unit,
                                       isForeground: Boolean)
}