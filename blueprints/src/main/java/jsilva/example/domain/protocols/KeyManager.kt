package jsilva.example.domain.protocols

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import java.security.InvalidAlgorithmParameterException
import java.security.NoSuchAlgorithmException
import java.security.NoSuchProviderException

interface KeyManager {

    @Throws(NoSuchProviderException::class, NoSuchAlgorithmException::class, InvalidAlgorithmParameterException::class, RuntimeException::class)
    fun createKeys(context: Context, alias: String)

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    fun createKeysMR2(context: Context, alias: String)

    @TargetApi(Build.VERSION_CODES.M)
    fun createKeysM(alias: String, requireAuth: Boolean)

    fun isSigningKey(alias: String) : Boolean

    fun encrypt(alias: String, plainText: String) : String
    fun decrypt(alias: String, cipherText: String) : String?

}