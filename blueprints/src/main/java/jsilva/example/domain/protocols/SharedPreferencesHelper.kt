package jsilva.example.domain.protocols

interface SharedPrefsHelper {

    fun put(key: String, value: String)

    fun put(key: String, value: Int)

    fun put(key: String, value: Float)

    fun put(key: String, value: Boolean)

    fun getString(key: String, defaultValue: String?): String?

    fun getInt(key: String, defaultValue: Int?): Int?

    fun getFloat(key: String, defaultValue: Float?): Float?

    fun getBoolean(key: String, defaultValue: Boolean): Boolean

    fun deleteSavedData(key: String)

}