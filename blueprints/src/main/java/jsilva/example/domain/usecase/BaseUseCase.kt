package jsilva.example.domain.usecase

abstract class BaseUseCase<R, P> {

    abstract suspend fun run(params: P): R

}