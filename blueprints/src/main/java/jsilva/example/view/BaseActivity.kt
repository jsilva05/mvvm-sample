package jsilva.example.view

import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import jsilva.example.blueprints.BuildConfig
import jsilva.example.domain.exception.BaseException
import jsilva.example.view.helpers.ErrorHandler
import jsilva.example.view.helpers.PopupHelper
import javax.inject.Inject

abstract class BaseActivity : AppCompatActivity(), HasSupportFragmentInjector {

    private val mPopupHelper = PopupHelper()

    @Inject
    internal lateinit var injector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = injector

    /**
     * The layout's resource identifier.
     * @return layout resource id
     */
    @LayoutRes abstract fun getLayoutId(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        setContentView(getLayoutId())
    }

    override fun onDestroy() {
        super.onDestroy()
        mPopupHelper.clearPendingPopups()
    }

    abstract fun getProgressBarId() : Int?

    fun toggleLoading(isLoading: Boolean) {

        val progressBarId = getProgressBarId()

        if (progressBarId != null) {
            val progressBar = findViewById<View>(progressBarId)

            if (progressBar != null) {

                if (isLoading) {
                    toggleUserInteractions(true)
                    progressBar.visibility = View.VISIBLE
                } else {
                    toggleUserInteractions(false)
                    progressBar.visibility = View.GONE
                }

            } else {
                throw RuntimeException("Add a ProgressBar to activity to use loading")
            }
        }
    }

    fun showErrorMessage(errorHandler: ErrorHandler) {
        if (errorHandler.error != null)
            mPopupHelper.showErrorPopup(this, body = getErrorMessage(errorHandler.error), dismissListener = errorHandler.dismissAction)
        else
            logWarning("Could not show error popup. Error Handler is null")
    }

    private fun getErrorMessage(error: Throwable) : String? {

        return when {
            error is BaseException -> {

                if (!error.errorMessage.nullOrTrimEmpty())
                    error.errorMessage
                else if (error.errorMessageId != null)
                    getString(error.errorMessageId)
                else
                    null
            }
            BuildConfig.DEBUG -> error.message ?: error.localizedMessage ?: error.toString()
            else -> null
        }
    }

    /**
     * Locks or unlocks the user's interactions with the window.
     * @param lock True to lock, false to unlock.
     */
    private fun toggleUserInteractions(lock: Boolean) {
        if (lock) {
            window?.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        } else {
            window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }
    }
}