package jsilva.example.view

import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.Toast
import timber.log.Timber

fun Context.notifyNotImplemented() = Toast.makeText(this, "Not implemented yet!", Toast.LENGTH_SHORT).show()

inline fun Context.runAsync(crossinline task : () -> Unit?) = Thread(Runnable { task() }).start()

inline fun View.runOnUI(crossinline  task: () -> Unit?) = this.post { task() }

fun Context.startActivityWithoutHistory(intent: Intent) {
    val newIntent = intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
    startActivity(newIntent)
}

fun Context.startActivityAndClearStack(intent: Intent) {
    val newIntent = intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK);
    startActivity(newIntent)
}

fun Any.logDebug(message: String) {
    Timber.d("${this::class.java} - $message")
}

fun Any.logError(message: String, throwable: Throwable? = null) {
    if (throwable != null) {
        Timber.e(throwable, "${this::class.java} - $message")
    } else Timber.e(message)
}

fun Any.logWarning(message: String) {
    Timber.w("${this::class.java} - $message")
}

fun Any.logInfo(message: String) {
    Timber.i("${this::class.java} - $message")
}

fun String?.nullOrTrimEmpty() = this == null || this.trim().isEmpty()