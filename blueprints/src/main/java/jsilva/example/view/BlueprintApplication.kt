package jsilva.example.view

import android.app.Application
import timber.log.Timber

open class BlueprintApplication : Application(){

    override fun onCreate() {
        super.onCreate()

        Timber.uprootAll()
        Timber.plant(Timber.DebugTree())
    }
}