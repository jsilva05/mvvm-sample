package jsilva.example.view.adapters

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

class BaseViewHolder<VB : ViewDataBinding, M>
constructor(internal val binding : VB,
            private val bindingVariable : Int,
            private val onItemClickListener: ItemListener<M>)
    : RecyclerView.ViewHolder(binding.root){

    fun bind(model : M){
        binding.setVariable(bindingVariable, model)
        binding.root.setOnClickListener { onItemClickListener.onItemClicked(model) }
        binding.executePendingBindings()
    }
}

interface ItemListener<M> {
    fun onItemClicked(model : M)
}