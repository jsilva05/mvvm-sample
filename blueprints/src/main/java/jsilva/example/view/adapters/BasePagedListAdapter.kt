package jsilva.example.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.paging.PagedList
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil

abstract class BasePagedListAdapter<VB: ViewDataBinding, M: Any>(diff: DiffUtil.ItemCallback<M>)
    : PagedListAdapter<M, BaseViewHolder<VB, M>>(diff), ItemListener<M> {

    abstract fun getBindingVariable() : Int

    private var mDataset: List<M> = emptyList()

    @LayoutRes
    abstract fun getViewLayoutResourceId() : Int

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<VB, M> {
        val inflater = LayoutInflater.from(parent.context)

        val binding : VB = DataBindingUtil.inflate(inflater, getViewLayoutResourceId(), parent, false)
        return BaseViewHolder(binding, getBindingVariable(), this)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<VB, M>, position: Int) {
        val item = getItem(position)
        if (item != null)
            holder.bind(item)
    }

    fun swapDataset(dataset : PagedList<M>) {
        mDataset = dataset

        submitList(dataset)
    }

}