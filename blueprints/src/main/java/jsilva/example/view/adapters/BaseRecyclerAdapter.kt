package jsilva.example.view.adapters

import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil

abstract class BaseRecyclerAdapter<VB : ViewDataBinding, M: Any>
    : RecyclerView.Adapter<BaseViewHolder<VB, M>>(), ItemListener<M>{

    var mDataset : List<M> = emptyList()

    @LayoutRes abstract fun getViewLayoutResourceId() : Int

    abstract fun getBindingVariable() : Int
    abstract fun areItemsTheSame(oldItem: M, newItem: M): Boolean
    abstract fun areContentsTheSame(oldItem: M, newItem: M): Boolean

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<VB, M> {
        val inflater = LayoutInflater.from(parent.context)

        val binding : VB = DataBindingUtil.inflate(inflater, getViewLayoutResourceId(), parent, false)
        return BaseViewHolder(binding, getBindingVariable(), this)

    }

    override fun getItemCount(): Int = mDataset.size


    override fun onBindViewHolder(holderBase: BaseViewHolder<VB, M>, position: Int) {
        holderBase.bind(mDataset[position])
    }

    override fun onViewRecycled(holderBase: BaseViewHolder<VB, M>) {
        holderBase.binding.invalidateAll()
        super.onViewRecycled(holderBase)
    }

    fun swapDataset(dataset : List<M>) {
        when {
            mDataset.isEmpty() && dataset.isNotEmpty() -> {
                mDataset = dataset
                notifyItemRangeInserted(0, dataset.size)
            }
            dataset.isEmpty() -> {
                notifyItemRangeRemoved(0, mDataset.size)
                mDataset = dataset
            }
            else -> calculateListDiff(dataset)
        }
    }

    private fun calculateListDiff(dataset: List<M>) {
        val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return areItemsTheSame(mDataset[oldItemPosition], dataset[newItemPosition])
            }

            override fun getOldListSize() = mDataset.size

            override fun getNewListSize() = dataset.size

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return areContentsTheSame(mDataset[oldItemPosition], dataset[newItemPosition])
            }

        })
        mDataset = dataset
        result.dispatchUpdatesTo(this)
    }
}

