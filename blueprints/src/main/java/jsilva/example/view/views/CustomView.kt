package jsilva.example.view.views

import android.util.AttributeSet

interface CustomView {

    fun init()

    fun handleAttributeSet(attrs : AttributeSet)
}