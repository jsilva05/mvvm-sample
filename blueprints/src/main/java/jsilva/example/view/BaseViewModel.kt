package jsilva.example.view

import androidx.lifecycle.*
import kotlinx.coroutines.*
import jsilva.example.domain.protocols.UseCaseExecutor
import jsilva.example.domain.usecase.BaseUseCase
import jsilva.example.view.helpers.ErrorHandler

abstract class BaseViewModel: ViewModel(), LifecycleObserver, UseCaseExecutor, CoroutineScope {

    val isLoading : MutableLiveData<Boolean>  = MutableLiveData()
    val errorHandler : MutableLiveData<ErrorHandler> = MutableLiveData()

    private fun startLoading() = isLoading.postValue(true)
    private fun stopLoading() = isLoading.postValue(false)

    override val coroutineContext = Job() + Dispatchers.Default

    override fun errorHandler(isForeground: Boolean, dismissListener: (() -> Unit)?): (Throwable) -> Unit {
        return {
            errorHandler.postValue(ErrorHandler(it, dismissListener))
        }
    }

    override fun onCompleteHandler(isForeground: Boolean): () -> Unit {
        return {
            if (isForeground)
                stopLoading()
        }
    }

    override fun <T, P> executeUseCase(useCase: BaseUseCase<T, P>,
                                       input: P,
                                       onSuccess: (T) -> Unit,
                                       onError: (Throwable) -> Unit,
                                       onComplete: () -> Unit,
                                       isForeground: Boolean) {

        if (isForeground) startLoading()

        val handler = CoroutineExceptionHandler { _, exception ->
            onError(exception)
            onComplete()
        }

        launch(handler) {

            val result = useCase.run(input)
            onSuccess(result)

            onComplete()
        }
    }

}

