package jsilva.example.view


import android.content.Context
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import dagger.android.support.AndroidSupportInjection

abstract class BaseFragment<VB : ViewDataBinding, VM : BaseViewModel> : Fragment() {

    protected lateinit var mViewDataBinding: VB

    /**
     * The layout's resource identifier.
     * @return layout resource id
     */
    @LayoutRes
    abstract fun getLayoutId(): Int

    /**
     * Enables access to the view's [ViewDataBinding] specification.
     * @return The view's [ViewDataBinding] specification.
     */
    fun getViewDataBinding(): VB = mViewDataBinding

    /**
     * Override for set view model
     * @return view model instance
     */
    abstract fun getViewModel(): VM?

    /**
     * Override for set binding variable
     * @return variable id
     */
    @LayoutRes
    abstract fun getBindingVariable(): Int

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        mViewDataBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        return mViewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewModel = getViewModel()

        if (viewModel != null) {

            mViewDataBinding.setVariable(getBindingVariable(), viewModel)
            mViewDataBinding.setLifecycleOwner(this)
            mViewDataBinding.executePendingBindings()

            lifecycle.addObserver(viewModel)

            viewModel.errorHandler.observe(this, Observer {
                if (activity != null && activity is BaseActivity)
                    (activity as BaseActivity).showErrorMessage(it)
            })

            viewModel.isLoading.observe(this, Observer {
                if (activity != null && activity is BaseActivity)
                    (activity as BaseActivity).toggleLoading(it)
            })
        }
    }
}