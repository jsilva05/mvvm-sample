package jsilva.example.view

import androidx.databinding.BindingAdapter
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions

private val requestOptions = RequestOptions()
        .placeholder(android.R.drawable.ic_menu_gallery)
        .centerCrop()


@BindingAdapter("imageUrl")
fun setImageUrl(view : ImageView, url : String?) = Glide.with(view.context)
        .load(url)
        .apply(requestOptions)
        .transition(DrawableTransitionOptions.withCrossFade())
        .into(view)
