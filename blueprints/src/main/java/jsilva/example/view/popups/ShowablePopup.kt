package jsilva.example.view.popups

import android.content.Context
import androidx.annotation.LayoutRes
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.PopupWindow

abstract class ShowablePopup
constructor(private val context : Context, width : Int, height : Int): PopupWindow() {

    private var rootView: View? = null


    init {
        isOutsideTouchable = true
        isFocusable = true

        val contentView = getDefaultPopupView()
        this.contentView = contentView
        this.width  = width
        this.height = height

        bindViews(contentView)
        setupEventListeners()

        rootView?.let {
            it.setOnKeyListener { _, _, _ -> false }
        }
    }



    @LayoutRes
    protected abstract fun getLayoutResourceId(): Int

    protected abstract fun bindViews(contentView: View)

    protected abstract fun setupEventListeners()

    protected fun getDefaultPopupView() : View{
        return LayoutInflater.from(context).inflate(getLayoutResourceId(), null)
    }

    fun show(anchor: View) {
        val location = IntArray(2)
        anchor.getLocationOnScreen(location)
        anchor.post { showAtLocation(anchor, Gravity.CENTER_VERTICAL, location[0], location[1] - anchor.height / 2) }

    }

    fun show(anchor: View, location: IntArray) {
        anchor.getLocationOnScreen(location)
        anchor.post { showAtLocation(anchor, Gravity.CENTER_VERTICAL, location[0], location[1]) }
    }

}