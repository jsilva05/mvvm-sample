package jsilva.example.view.helpers

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.view.View
import android.widget.TextView
import jsilva.example.blueprints.R
import jsilva.example.view.nullOrTrimEmpty

class PopupHelper {

    private val registeredPopups : MutableList<Dialog> = mutableListOf()

    fun registerPopup(popup: Dialog) = registeredPopups.add(popup)

    fun unregisterPopup(popup: Dialog) = registeredPopups.remove(popup)

    fun clearPendingPopups(){
        for(popup in registeredPopups)
            if(popup.isShowing) popup.dismiss()

        registeredPopups.clear()
    }


    fun showErrorPopup(context: Activity?, title: String? = null, body: String? = null, dismissListener: (() -> Unit)? = null) {

        val dialogBuilder = AlertDialog.Builder(context)
        val inflater = context?.layoutInflater

        if (inflater != null) {

            val view = inflater.inflate(R.layout.error_popup, null)
            dialogBuilder.setView(view)

            val dialog = dialogBuilder.create()
            registerPopup(dialog)

            setupErrorText(context, view, title, body)
            setupListeners(dialog, view)

            if (dismissListener != null)
                dialog.setOnDismissListener {
                    dismissListener()
                }

            if (!context.isFinishing)
                dialog.show()

        }

    }

    private fun setupListeners(dialog: AlertDialog, view: View) {

        dialog.setOnDismissListener {
            unregisterPopup(dialog)
        }

        view.setOnClickListener {
            dialog.dismiss()
        }
    }

    private fun setupErrorText(context: Context, view: View, title: String?, body: String?) {
        val tvTitle = view.findViewById<TextView>(R.id.tv_title)
        val tvBody = view.findViewById<TextView>(R.id.tv_body)

        if (!title.nullOrTrimEmpty())
            tvTitle.text = title
        else
            tvTitle.text = context.getString(R.string.generic_error_title)

        if (!body.nullOrTrimEmpty())
            tvBody.text = body
        else
            tvBody.text = context.getString(R.string.generic_error_body)
    }
}