package jsilva.example.view.helpers

class ErrorHandler(
        val error: Throwable?,
        val dismissAction: (() -> Unit)?
)