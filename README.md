This project aims to use the following components:
- Dagger
- Kotlin Coroutines
- Databinding
- Navigation Architecture
- Paging Library
- Room
- Retrofit
